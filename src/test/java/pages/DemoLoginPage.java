package pages;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.qameta.allure.Step;
import org.junit.Assert;
import org.openqa.selenium.By;


public class DemoLoginPage extends BasePage {

    public DemoLoginPage(AndroidDriver<AndroidElement> driver)
    {
        super(driver);
    }

    ///element locators
    public String topLogoId = "top_logo";
    public String versionLabelId  = "version_label";
    public void tapAndHoldOnVersionLabel(){longPress(By.id(versionLabelId));}
    public String posIdLabelId = "id_label";
    public String clockDateId = "clock_date";
    public String clockTimeId = "clock_time";
    public String wallpaperIconId = "background_options";
    public String passwordFieldId = "pin_input";//id
    public String loginFromKeyOKId = "keyOk";
    public String loginFormKeyCancelId = "keyCanc";
    public String loginFormKeyNumber0Id = "key0";//id
    public String loginFormKeyNumber1Id = "key1";//id
    public String loginFormKeyNumber2Id = "key2";//id
    public String loginFormKeyNumber3Id = "key3";//id
    public String loginFormKeyNumber4Id = "key4";//id
    public String loginFormKeyNumber5Id = "key5";//id
    public String loginFormKeyNumber6Id = "key6";//id
    public String loginFormKeyNumber7Id = "key7";//id
    public String loginFormKeyNumber8Id = "key8";//id
    public String loginFormKeyNumber9Id = "key9";//id
    public String manualRestartConfirmationOkButtonId = "button3";

    public String logfileCloseBtnXpath = "//android.widget.Button[@text='close']";
    public void selectClose() {click(By.xpath(logfileCloseBtnXpath));}
    public String logfileEmailBtnXpath = "//android.widget.Button[@text='email']";
    public void selectEmail() {click(By.xpath(logfileEmailBtnXpath));}


    @Step("press on key 0")
    public void pressKeyNumber0(){click(By.id(loginFormKeyNumber0Id));}
    @Step("press on key 1")
    public void pressKeyNumber1(){click(By.id(loginFormKeyNumber1Id));}
    @Step("press on key 2")
    public void pressKeyNumber2(){click(By.id(loginFormKeyNumber2Id));}
    @Step("press on key 3")
    public void pressKeyNumber3(){click(By.id(loginFormKeyNumber3Id));}
    @Step("press on key 4")
    public void pressKeyNumber4(){click(By.id(loginFormKeyNumber4Id));}
    @Step("press on key 5")
    public void pressKeyNumber5(){click(By.id(loginFormKeyNumber5Id));}
    @Step("press on key 6")
    public void pressKeyNumber6(){click(By.id(loginFormKeyNumber6Id));}
    @Step("press on key 7")
    public void pressKeyNumber7(){click(By.id(loginFormKeyNumber7Id));}
    @Step("press on key 8")
    public void pressKeyNumber8(){click(By.id(loginFormKeyNumber8Id));}
    @Step("press on key 9")
    public void pressKeyNumber9(){click(By.id(loginFormKeyNumber9Id));}
    @Step("press on key OK")
    public void pressKeyOK(){click(By.id(loginFromKeyOKId));}
    public void pressKeycancel(){click(By.id(loginFormKeyCancelId));}

}
