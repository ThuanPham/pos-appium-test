package scenarios;

import org.testng.annotations.*;
import test.pholser.Demo;
import utils.ExtentReports.ExtentTestManager;
import pages.DemoLoginPage;
import utils.Listeners.TestListener;

@Listeners({TestListener.class})
public class DemoLoginTest extends AndroidSetup{

    @Parameters(value = "apkFilePath")
    @BeforeClass
    public void setUp(String apkFilePath) throws Exception {
        prepareAndroidForAppium(apkFilePath);
    }

    @AfterClass
    public void tearDown() throws Exception {
        driver.quit();

    }

    @Test (priority = 1, description = "this is my first test")
    public void loginWithRightPass() throws InterruptedException {
        DemoLoginPage demoLoginPage = new DemoLoginPage(driver);
        demoLoginPage.pressKeyNumber1();
        demoLoginPage.pressKeyNumber2();
        demoLoginPage.pressKeyNumber3();
        demoLoginPage.pressKeyNumber4();
    }
}
